#include <iostream>
#include "list"
#include "vector"
#include "memory"
#include "unordered_set"
#include "stdio.h"
#include "algorithm"
#include "queue"
#include "exception"
#include "regex"
#include "unordered_set"
#include "map"


template<typename Vertex, typename VertexValue,
        typename Edge, typename EdgeValue>
class GraphADT {
protected:
    [[maybe_unused]] virtual std::shared_ptr<Vertex> addVertex(const VertexValue &) = 0;

    [[maybe_unused]] virtual void removeVertex(std::shared_ptr<Vertex>) = 0;

    [[maybe_unused]] virtual std::shared_ptr<Edge>
    addEdge(std::shared_ptr<Vertex> from, std::shared_ptr<Vertex> to, const EdgeValue &) = 0;

    [[maybe_unused]] virtual void removeEdge(std::shared_ptr<Edge>) = 0;

    [[maybe_unused]] [[nodiscard]] virtual std::list<std::shared_ptr<Edge>>
    edgesFrom(std::shared_ptr<Vertex>) const = 0;

    [[maybe_unused]] [[nodiscard]] virtual std::list<std::shared_ptr<Edge>>
    edgesTo(std::shared_ptr<Vertex>) const = 0;

    [[maybe_unused]] [[nodiscard]] virtual std::shared_ptr<Vertex> findVertex(const VertexValue &) const = 0;

    [[maybe_unused]] [[nodiscard]] virtual std::shared_ptr<Edge>
    findEdge(const VertexValue &from, const VertexValue &to) const = 0;

    [[maybe_unused]] [[nodiscard]] virtual bool
    hasEdge(std::shared_ptr<Vertex> from, std::shared_ptr<Vertex> to) const = 0;
};


template<typename V>
class Vertex {
private:
    V value;
public:
    size_t id{};

    [[maybe_unused]] explicit Vertex(const V &val, size_t id = -1) {
        value = val;
        this->id = id;
    }

    [[maybe_unused]] [[nodiscard]] const V &getVal() const { return value; }

    [[maybe_unused]] void setVal(const V &&val) const { this->val = std::move(val); }
};


template<typename V, typename W>
class Edge {
private:
    W weight;
    std::shared_ptr<V> fromVertex, toVertex;

public:
    [[maybe_unused]] Edge(std::shared_ptr<V> from, std::shared_ptr<V> to, const W &w) {
        weight = w;
        fromVertex = std::shared_ptr<V>(from);
        toVertex = std::shared_ptr<V>(to);
    }

    [[maybe_unused]] Edge(std::shared_ptr<V> from, std::shared_ptr<V> to) {
        fromVertex = std::shared_ptr<V>(from);
        toVertex = std::shared_ptr<V>(to);
    }

    Edge(const V &from, const V &to) {
        fromVertex = std::shared_ptr<const V>(&from);
        toVertex = std::shared_ptr<const V>(&to);
    }

    [[maybe_unused]] [[nodiscard]] W getWeight() const { return weight; }

    [[maybe_unused]] [[nodiscard]] std::shared_ptr<V> from() const { return fromVertex; }

    [[maybe_unused]] [[nodiscard]] std::shared_ptr<V> to() const { return toVertex; }

    void transpose() {
        fromVertex.swap(toVertex);
    }
};


template<typename VertexValue, typename EdgeValue>
class AdjacencyMatrixGraph : public GraphADT<Vertex<VertexValue>, VertexValue,
        Edge<Vertex<VertexValue>, EdgeValue>, EdgeValue> {
private:
    using V = Vertex<VertexValue>;
    using E = Edge<V, EdgeValue>;

    struct vertex_hash {
        std::size_t operator()(const std::shared_ptr<V> pv) const {
            return std::hash<VertexValue>{}(pv->getVal());
        }
    };

    struct vertex_eq {
        bool operator()(const std::shared_ptr<V> p1,
                        const std::shared_ptr<V> p2) const {
            return p1->getVal() == p2->getVal();
        }
    };

    struct edge_hash {
        std::size_t operator()(const std::shared_ptr<E> pe) const {
            size_t hash = std::hash<VertexValue>{}(pe->from()->getVal()) + \
                std::hash<VertexValue>{}(pe->to()->getVal());
//                std::hash<EdgeValue>{}(pe->getWeight());
            return hash;
        }
    };

    struct edge_eq {
        bool operator()(const std::shared_ptr<E> e1,
                        const std::shared_ptr<E> e2) const {
            return e1->from() == e2->from() && e1->to() == e2->to();
        }
    };

    class IndexProvider {
    private:
        std::vector<size_t> emptyIndexes;
    public:
        explicit IndexProvider(int size) {
            emptyIndexes = std::vector<size_t>(size);
            for (size_t i = 0; i < size; i++) emptyIndexes[i] = i;
        }

        IndexProvider() = default;

        size_t get() {
            size_t ind = emptyIndexes.back();
            emptyIndexes.pop_back();
            return ind;
        }

        // User should check that retrieved index is not a copy!
        void retrieve(size_t index) {
            emptyIndexes.push_back(index);
        }
    };

    [[maybe_unused]] [[nodiscard]] std::shared_ptr<V> getPointerToVertex(const VertexValue &val) const {
        auto search = setOfVertices.find(std::make_shared<V>(val));
        if (search != setOfVertices.end()) return *search;
        return nullptr;
    }

    std::vector<std::vector<std::shared_ptr<E>>> adjacencyMatrix;
    IndexProvider vertexIndexer;
public:
    std::unordered_multiset<std::shared_ptr<V>, vertex_hash, vertex_eq> setOfVertices;
    std::unordered_multiset<std::shared_ptr<E>, edge_hash, edge_eq> setOfEdges;


    int size{};

    explicit AdjacencyMatrixGraph(const int size) {
        adjacencyMatrix = std::vector(size, std::vector<std::shared_ptr<E>>(size, nullptr));
        vertexIndexer = IndexProvider(size);
        this->size = size;
    }

    [[maybe_unused]] std::shared_ptr<V> addVertex(const VertexValue &val) override {
        auto vertexToInsert = std::make_shared<V>(val, vertexIndexer.get());
        setOfVertices.insert(vertexToInsert);
        return vertexToInsert;
    }

    [[maybe_unused]] void removeVertex(std::shared_ptr<V> vertex) override {
        for (size_t i = 0; i < size; i++)
            adjacencyMatrix[vertex->id][i] = adjacencyMatrix[i][vertex->id] = nullptr;
        vertexIndexer.retrieve(vertex->id);
        setOfVertices.erase(vertex);
    }

    [[maybe_unused]] std::shared_ptr<E>
    addEdge(std::shared_ptr<V> from, std::shared_ptr<V> to, const EdgeValue &val) override {
        auto edge = std::make_shared<E>(from, to, val);
        setOfEdges.insert(edge);
        adjacencyMatrix[from->id][to->id] = edge;
        return edge;
    }

    [[maybe_unused]] void removeEdge(std::shared_ptr<E> edge) override {
        setOfEdges.erase(edge);
        adjacencyMatrix[edge->from()->id][edge->to()->id] = nullptr;
    }

    [[maybe_unused]] [[nodiscard]] std::list<std::shared_ptr<E>>
    edgesFrom(std::shared_ptr<V> ver) const override {
        auto edges = std::list<std::shared_ptr<E>>();
        for (auto &e : adjacencyMatrix[ver->id])
            if (e != nullptr) edges.push_back(e);
        return edges;
    }

    [[maybe_unused]] [[nodiscard]] std::list<std::shared_ptr<E>>
    edgesTo(std::shared_ptr<V> ver) const override {
        auto edges = std::list<std::shared_ptr<E>>();
        for (size_t i = 0; i < size; i++)
            if (adjacencyMatrix[i][ver->id] != nullptr)
                edges.push_back(adjacencyMatrix[i][ver->id]);
        return edges;
    }

    [[maybe_unused]] [[nodiscard]] std::shared_ptr<V> findVertex(const VertexValue &val) const override {
        auto search = setOfVertices.find(std::make_shared<V>(val));
        if (search != setOfVertices.end()) return *search;
        return nullptr;
    }

    // use optional
    [[maybe_unused]] [[nodiscard]] std::shared_ptr<E>
    findEdge(const VertexValue &from, const VertexValue &to) const override {
        auto search = setOfEdges.find(std::make_shared<E>(getPointerToVertex(from), getPointerToVertex(to)));
        if (search != setOfEdges.end()) return *search;
        return nullptr;
    }

    [[maybe_unused]] bool hasEdge(std::shared_ptr<V> from, std::shared_ptr<V> to) const override {
        auto edges = edgesFrom(from);
        for (const auto &e : edges) {
            if (e->to() == to) {
                return true;
            }
        }
        return false;
    };

    void transpose() {
        for (int i = 0; i < size; i++)
            for (int j = 0; j < i; j++)
                adjacencyMatrix[j][i].swap(adjacencyMatrix[i][j]);
        for (auto it = setOfEdges.begin(); it != setOfEdges.end(); it++) (*it)->transpose();
    }
};


class InvalidInput : public std::exception {
};

class InputFileIsMalformed : public InvalidInput {
public:
    [[maybe_unused]] [[nodiscard]] const char *what() const noexcept override {
        return "E0: Input file is malformed";
    }
};

class InitialStateIsNotDefined : public InvalidInput {
public:
    [[maybe_unused]] [[nodiscard]] const char *what() const noexcept override {
        return "E4: Initial state is not defined";
    }
};

class TransitionIsNotRepresentInTheAlphabet : public InvalidInput {
private:
    std::string trans;
public:
    explicit TransitionIsNotRepresentInTheAlphabet(const std::string &t) : InvalidInput() {
        trans = t;
    }

    [[maybe_unused]] [[nodiscard]] const char *what() const noexcept override {
        auto msg = new std::string("E3: A transition '" + trans + "' is not represented in the alphabet");
        return msg->c_str();
    }
};

class StateIsNotInTheSetOfStates : public InvalidInput {
private:
    std::string state;
public:
    explicit StateIsNotInTheSetOfStates(const std::string &s) : InvalidInput() {
        state = s;
    }

    [[maybe_unused]] [[nodiscard]] const char *what() const noexcept override {
        auto msg = new std::string("E1: A state '" + state + "' is not in the set of states");
        return msg->c_str();
    }
};


class InvalidFSA : public std::exception {
};

class FSAIsNonDeterministic : public InvalidFSA {
public:
    [[maybe_unused]] [[nodiscard]] const char *what() const noexcept override {
        return "E5: FSA is nondeterministic";
    }
};

class HasDisjointStates : public InvalidFSA {
public:
    [[maybe_unused]] [[nodiscard]] const char *what() const noexcept override {
        return "E2: Some states are disjoint";
    }
};

std::vector<std::string>
parseInputArray(const std::string &prefix, const std::string &array, const std::string &itemRegex) {
    std::regex items_regex;
    if (prefix == "trans") items_regex = std::regex(prefix + "=\\[(" + itemRegex + ")(,(" + itemRegex + "))*\\]");
    else items_regex = std::regex(prefix + "=\\[(" + itemRegex + "*)(,(" + itemRegex + "+))*\\]");

    if (!std::regex_match(array, items_regex)) throw InputFileIsMalformed();

    std::vector<std::string> items;
    size_t prevInd = prefix.size() + 2;
    for (size_t i = prefix.size() + 2; i < array.size(); i++) {
        if (array[i] == ',') {
            items.push_back(array.substr(prevInd, i - prevInd));
            prevInd = i + 1;
        }
    }
    items.push_back(array.substr(prevInd, array.size() - prevInd - 1));

    return items;
}

struct FSANode {
    inline static int globalId = 0;
    enum class Type {
        INITIAL = 1, ACCEPTING = 2, INITIAL_AND_ACCEPTING = 3, NOTHING = 4
    };
    int id;

    std::string name{};
    Type type{};

    FSANode() = default;

    explicit FSANode(const std::string &n) { name = n; }

    FSANode(const std::string &n, Type t) {
        name = n;
        type = t;
        id = globalId++;
    }

    bool operator==(const std::string &n) const { return n == name; }

    bool operator==(const FSANode &node) const { return node.name == name; }
};


using FSA = AdjacencyMatrixGraph<FSANode, std::string>;

std::vector<std::string> parseTrans(const std::string &transition) {
    std::vector<std::string> names;
    int prev_ind = 0;
    for (int i = 0; i < transition.size(); i++) {
        if (transition[i] == '>') {
            names.push_back(transition.substr(prev_ind, i - prev_ind));
            prev_ind = i + 1;
        }
    }
    names.push_back(transition.substr(prev_ind, names.size() - prev_ind + 1));
    return names;
};

namespace std {
    template<>
    struct hash<FSANode> {
        std::size_t operator()(const FSANode &node) const {
            return std::hash<std::string>()(node.name);
        }
    };
}

std::istream &operator>>(std::istream &is, std::unique_ptr<FSA> &fsa) {
    std::string statesInp, alphaInp, initialInp, acceptingInp, transInp;
    is >> statesInp >> alphaInp >> initialInp >> acceptingInp >> transInp;

    // Regexp for items of each list
    static std::string stateItemRegex = R"([a-zA-Z0-9 ])";
    static std::string alphaItemRegex = R"([a-zA-Z0-9_ ])";
    std::string transItemRegex = stateItemRegex + "+>" + alphaItemRegex + "+>" + stateItemRegex + "+";

    // Parsing input lists into sets
    auto getItemSet = [](const auto &items) { return std::unordered_set(items.begin(), items.end()); };
    auto states = parseInputArray("states", statesInp, stateItemRegex);
    auto alphaSet = getItemSet(parseInputArray("alpha", alphaInp, alphaItemRegex));

    // Extracting initial state
    auto initialStates = parseInputArray("initial", initialInp, stateItemRegex);

    if (initialStates.size() != 1) throw InitialStateIsNotDefined();
    std::string initialState = initialStates[0];

    auto acceptingSet = getItemSet(parseInputArray("accepting", acceptingInp, alphaItemRegex));

    // returns type of state accordingly to the initial and accepting states
    auto getNodeType = [&](const std::string &name) {
        if (name == initialState) {
            if (acceptingSet.find(name) != acceptingSet.end()) return FSANode::Type::INITIAL_AND_ACCEPTING;
            return FSANode::Type::INITIAL;
        }
        if (acceptingSet.find(name) != acceptingSet.end()) return FSANode::Type::ACCEPTING;
        return FSANode::Type::NOTHING;
    };

    // Adding states in FSA Graph and checking that the initial state is present
    for (const auto &state : states) fsa->addVertex(FSANode(state, getNodeType(state)));

    auto stateSet = std::unordered_set(states.begin(), states.end());

    // Inserting transitions in the graph
    for (const auto &trans : parseInputArray("trans", transInp, transItemRegex)) {
        auto names = parseTrans(trans);
        if (alphaSet.find(names[1]) == alphaSet.end()) throw TransitionIsNotRepresentInTheAlphabet(names[1]);
        if (stateSet.find(names[0]) == stateSet.end()) throw StateIsNotInTheSetOfStates(names[0]);
        if (stateSet.find(names[2]) == stateSet.end()) throw StateIsNotInTheSetOfStates(names[2]);

        auto verFrom = fsa->findVertex(FSANode(names[0]));
        auto verTo = fsa->findVertex(FSANode(names[2]));
        if (fsa->hasEdge(verFrom, verTo))
            fsa->addEdge(verFrom, verTo,
                         fsa->findEdge(FSANode(names[0]), FSANode(names[2]))->getWeight() + "|" + names[1]);
        else
            fsa->addEdge(verFrom, verTo, names[1]);
    }
    return is;
}


bool isDeterministic(const std::unique_ptr<FSA> &fsa) {
    for (const auto &ver : fsa->setOfVertices) {
        auto edgeSet = std::unordered_set<std::string>();
        for (const auto &edge : fsa->edgesFrom(ver)) {
            if (edgeSet.find(edge->getWeight()) != edgeSet.end()) return false;
            edgeSet.insert(edge->getWeight());
        }
    }
    return true;
}

void dfsOnUndirectedGraph(const std::unique_ptr<FSA> &fsa, const std::shared_ptr<Vertex<FSANode>> &ver,
                          std::unordered_set<std::string> &used) {
    used.insert(ver->getVal().name);
    for (const auto &edge: fsa->edgesFrom(ver)) {
        auto verName = edge->to()->getVal().name;
        if (used.find(verName) == used.end())
            dfsOnUndirectedGraph(fsa, edge->to(), used);
    }
    for (const auto &edge: fsa->edgesTo(ver)) {
        auto verName = edge->from()->getVal().name;
        if (used.find(verName) == used.end())
            dfsOnUndirectedGraph(fsa, edge->from(), used);
    }
};


std::string FSAToRegexp(const std::unique_ptr<FSA> &fsa) {
    using std::vector;
    using std::string;

    bool accPresent = false;
    for (const auto &state : fsa->setOfVertices)
        if (state->getVal().type == FSANode::Type::ACCEPTING ||
            state->getVal().type == FSANode::Type::INITIAL_AND_ACCEPTING) {
            accPresent = true;
            break;
        }
    if (!accPresent) return "{}";

    auto statesNum = FSANode::globalId;

    // R[k][i][j]
    auto R = vector<vector<vector<string>>>(statesNum + 1, vector<vector<string>>(statesNum,
                                                                                  vector<string>(statesNum)));

    auto updateState = [](string &s, const string &u) {
        if (s.empty()) s = u;
        else s += '|' + u;
    };

    for (const auto &state: fsa->setOfVertices) {
        for (const auto &trans: fsa->edgesFrom(state)) {
            updateState(R[0][state->getVal().id][trans->to()->getVal().id], trans->getWeight());
        }
        updateState(R[0][state->getVal().id][state->getVal().id], "eps");
    }

    auto getToken = [](const string &reg) {
        if (reg.length() == 0) return string("({})");
        return "(" + reg + ")";
    };

    for (int k = 1; k <= statesNum; k++) {
        for (int i = 0; i < statesNum; i++) {
            for (int j = 0; j < statesNum; j++) {
                R[k][i][j] = getToken(R[k - 1][i][k - 1]) + getToken(R[k - 1][k - 1][k - 1]) + "*" +
                             getToken(R[k - 1][k - 1][j]) + "|" + getToken(R[k - 1][i][j]);
            }
        }
    }

    int initialIndex;
    for (const auto &ver: fsa->setOfVertices) {
        if (ver->getVal().type == FSANode::Type::INITIAL_AND_ACCEPTING ||
            ver->getVal().type == FSANode::Type::INITIAL) {
            initialIndex = ver->getVal().id;
        }
    }

    string ans;
    for (const auto &state: fsa->setOfVertices)
        if (state->getVal().type == FSANode::Type::ACCEPTING) {
            if (!ans.empty()) ans += '|';
            ans += R[statesNum][initialIndex][state->getVal().id];
        }

    return ans.empty() ? "{}" : ans;
}


bool hasDisjointStates(const std::unique_ptr<FSA> &fsa) {
    auto used = std::unordered_set<std::string>();
    dfsOnUndirectedGraph(fsa, *fsa->setOfVertices.begin(), used);
    return used.size() != fsa->setOfVertices.size();
}

int main() {
    freopen("input.txt","r", stdin);
    freopen("output.txt","w", stdout);
    auto fsa = std::make_unique<FSA>(100);

    try {
        std::cin >> fsa;
    } catch (const InvalidInput &exc) {
        std::cout << "Error: \n" << exc.what() << '\n';
        return 0;
    }

    try {
        if (!isDeterministic(fsa)) throw FSAIsNonDeterministic();
        if (hasDisjointStates(fsa)) throw HasDisjointStates();
    } catch (const InvalidFSA &exc) {
        std::cout << "Error:\n" << exc.what() << '\n';
        return 0;
    }


    std::cout << FSAToRegexp(fsa) << '\n';

    return 0;
}